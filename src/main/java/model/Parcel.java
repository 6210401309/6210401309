package model;

import model.Mailbox;

public class Parcel extends Mailbox {

    private String service;
    private String trackingNumber;
    public Parcel(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail,String service,String trackingNumber) {
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail);
        this.service = service;
        this.trackingNumber = trackingNumber;
    }
    public String getService(){
        return service;
    }

    public  String getTrackingNumber(){
        return trackingNumber;
    }

    @Override
    public String getType() {
        return "Parcel";
    }
}
