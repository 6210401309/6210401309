package model;

import model.Document;

public class ReceiveDocument extends Document {
    private String nameGuest;
    private String nameStaffReceiveMail;
    private String receiveTime;
    public ReceiveDocument(String type, String recipient, int numRoom, String deliverer, String size, String nameStaffAddMail, String nameGuest, String nameStaffReceiveMail, String important, String receiveTime, String time){
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail,important);
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = receiveTime;
        super.setTime(time);
    }
    public ReceiveDocument(Document mail, String nameGuest, String nameStaffReceiveMail, String time){
        super(mail.getType(),mail.getRecipient(),mail.getNumRoom(),mail.getDeliverer(),mail.getSize(),mail.getNameStaffAddMail(),mail.getImportant());
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = time;
        super.setTime(mail.getTime());
    }
    public String getNameGuest(){return nameGuest;}

    public String getNameStaffReceiveMail(){return  nameStaffReceiveMail;}

    public String getReceiveTime() { return receiveTime; }
}
