package model;

import model.Parcel;

public class ReceiveParcel extends Parcel {
    private String nameGuest;
    private String nameStaffReceiveMail;
    private String receiveTime;
    public ReceiveParcel(String type, String recipient, int numRoom, String deliverer, String size, String nameStaffAddMail, String nameGuest, String nameStaffReceiveMail, String service, String trackingNumber, String receiveTime, String time){
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail,service,trackingNumber);
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = receiveTime;
        super.setTime(time);
    }
    public ReceiveParcel(Parcel mail, String nameGuest, String nameStaffReceiveMail, String time){
        super(mail.getType(),mail.getRecipient(),mail.getNumRoom(),mail.getDeliverer(),mail.getSize(),mail.getNameStaffAddMail(),mail.getService(),mail.getTrackingNumber());
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = time;
        super.setTime(mail.getTime());
    }


    //public String getTime() { return time; }
    public String getNameGuest(){return nameGuest;}

    public String getNameStaffReceiveMail(){return  nameStaffReceiveMail;}

    public String getReceiveTime() { return receiveTime; }
}
