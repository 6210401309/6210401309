package model;

public class Document extends Mailbox {

    private String important;

    public Document(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail,String important) {
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail);
        this.important = important;
    }

    public String getImportant(){
        return  important;
    }

    @Override
    public String getType() {
        return "Document";
    }

}
