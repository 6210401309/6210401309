package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Mailbox {
    private String type;
    private String recipient;
    private int numRoom;
    private String deliverer;
    private String size;
    private String nameStaffAddMail;
    private String time;
    private ArrayList<Mailbox> box;

    public  Mailbox(){
        box = new ArrayList<>();
    }
    public void addLetter(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail){
        Mailbox newLetter = new Letter(type,recipient,numRoom,deliverer,size,nameStaffAddMail);
        box.add(newLetter);
    }

    public void addDocument(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail,String important){
        Mailbox newDocument = new Document(type,recipient,numRoom,deliverer,size,nameStaffAddMail,important);
        box.add(newDocument);
    }

    public void addParcel(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail,String service,String  trackingNumber){
        Mailbox  newParcel = new Parcel(type,recipient,numRoom,deliverer,size,nameStaffAddMail,service,trackingNumber);
        box.add(newParcel);
    }
    public void add(Mailbox mail){
        box.add(mail);
    }
    public Mailbox(String type,String recipient,int numRoom,String deliverer, String size,String nameStaffAddMail){
        this.type = type;
        this.recipient = recipient;
        this.numRoom = numRoom;
        this.deliverer = deliverer;
        this.size = size;
        this.nameStaffAddMail = nameStaffAddMail;
    }
    public int compareTime(Mailbox o) throws ParseException {
        if(time == null){
            return -1;
        }
        if(o.time ==null){
            return 1;
        }
        Date thisDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(time);
        Date oDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(o.time);
        return thisDate.compareTo(oDate);
    }

    public String getRecipient(){
        return recipient;
    }

    public ArrayList<Mailbox> getBox() {
        return box;
    }

    public String getType(){
        return "Mail";
    }

    public int getNumRoom(){ return  numRoom; }

    public  String getDeliverer(){
        return deliverer;
    }

    public String getSize() {
        return size;
    }

    public String getNameStaffAddMail(){ return nameStaffAddMail; }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }


}

