package model;

import java.util.ArrayList;

public class Room {
    private String typeRoom;
    private int building;
    private int level;
    private int numRoom;
    private String name;
    private String username;
    private String password;
    private String checkPassword;
    private Room currentAccount;
    private ArrayList<Room> rooms;

    public Room(String typeRoom,int building,int level,int numRoom,String name,String username,String password,String checkPassword){
        this.typeRoom = typeRoom;
        this.building = building;
        this.level = level;
        this.numRoom = numRoom;
        this.name= name;
        this.username = username;
        this.password = password;
        this.checkPassword = checkPassword;
    }

    public Room(){
        rooms = new ArrayList<>();
    }
    public void addRoom(String typeRoom, int building,int level,int numRoom,String name,String username,String password,String checkPassword){
        Room newRoom = new Room(typeRoom,building,level,numRoom,name,username,password,checkPassword);
        rooms.add(newRoom);
    }



    public boolean used(int building,int level,int numRoom){
        for(Room room:rooms){
            if(building == room.building && level == room.level&& numRoom == room.numRoom){
                currentAccount = room;
                return false;
            }
        }
        return true;
    }

    public  Room getCurrentAccount() {  return currentAccount; }
    public void add(Room room) {
        rooms.add(room);
    }

    public String getTypeRoom() {
        return "Normal";
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public Integer getBuilding() { return building; }

    public Integer getLevel() { return level; }

    public Integer getNumRoom() {
        return numRoom;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return  password;
    }

    public String CheckPassword() {
        return checkPassword;
    }

    public void setName(String name) {
        this.name = name;
    }
}
