package model;

import model.Mailbox;

public class ReceiveLetter extends Mailbox {
    private String receiveTime;
    private String nameGuest;
    private String nameStaffReceiveMail;

    public ReceiveLetter(String type, String recipient, int numRoom, String deliverer, String size, String nameStaffAddMail, String nameGuest, String nameStaffReceiveMail, String receiveTime, String time){
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail);
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = receiveTime;
        super.setTime(time);
    }
    public ReceiveLetter(Mailbox mail, String nameGuest, String nameStaffReceiveMail, String time){
        super(mail.getType(),mail.getRecipient(),mail.getNumRoom(),mail.getDeliverer(),mail.getSize(),mail.getNameStaffAddMail());
        this.nameGuest = nameGuest;
        this.nameStaffReceiveMail = nameStaffReceiveMail;
        this.receiveTime = time;
        super.setTime(mail.getTime());
    }
    public String getNameGuest(){return nameGuest;}

    public String getNameStaffReceiveMail(){return  nameStaffReceiveMail;}

    public String getReceiveTime() { return receiveTime; }
}
