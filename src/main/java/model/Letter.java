package model;

public class Letter extends Mailbox {

    public Letter(String type,String recipient, int numRoom, String deliverer, String size,String nameStaffAddMail) {
        super(type,recipient, numRoom, deliverer, size,nameStaffAddMail);
    }

    @Override
    public String getType() { return "Letter"; }
}
