package model;

public class Extraroom extends Room {
    private String secondName;
    private String thirdName;
    public Extraroom(String typeRoom,int building,int level,int numRoom,String name,String username,String password,String checkPassword,String secondName,String thirdName){
        super(typeRoom,building,level,numRoom,name,username,password,checkPassword);
        this.secondName = secondName;
        this.thirdName = thirdName;
        }
    public String getSecondName() {
        return secondName;
    }
    public String getThirdName() {
        return thirdName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public String getTypeRoom() {
        return "Extra";
    }
}
