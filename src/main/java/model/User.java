package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class User {
    private String name;
    private String username;
    private String password;
    private User currentAccount;
    private String time;
    private ArrayList<User> userbox;

    public User(){
        userbox = new ArrayList<>();
    }
    public User(String name,String username, String password){
        this.name = name;
        this.username = username;
        this.password = password;
    }
    public String getName() {
        return name;
    }
    public boolean login(String checkusername,String checkPassword){
        for(User user:userbox){
            if(checkPassword.equals(user.password)&&checkusername.equals(user.username)){
                currentAccount = user;
                return true;
            }
        }
        return false;
    }
    public void addUser(String name,String username,String password){
        User newUser = new User(name,username,password);
        userbox.add(newUser);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getCurrentAccount() {
        return currentAccount;
    }

    public void add(User user){
        userbox.add(user);
     }

    public int compareTime(User o) throws ParseException {
        if(time==null){
            return -1;
        }
        Date thisDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(time);
        Date oDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(o.time);
        return thisDate.compareTo(oDate);
    }

    public ArrayList<User> getUserBox() {
        return userbox;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }
}
