package controller;

import javafx.scene.control.*;
import model.User;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StaffLogin {
    private UserFileDataSource userData;
    private User userList;
    private User user;
    @FXML
    Button homeBtnStaff,enterStaffBtn;
    @FXML
    TextField usernameTextFieldStaffPage;
    @FXML
    PasswordField passwordPasswordFieldStaffPage;

    public void setUser(User user) {
        this.user = user;
    }
    @FXML public void initialize(){
        userData = new UserFileDataSource("data","loginstaff.csv");
        userList= userData.getUserData();
    }
    @FXML
    public void homeBtnStaffOnAction(ActionEvent event) throws IOException {
        Button backHomeStaff = (Button) event.getSource();
        Stage stage = (Stage) backHomeStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome bhStaff = loader.getController();
        stage.show();
    }
    @FXML
    public void enterStaffBtnOnAction(ActionEvent event) throws IOException {
        userData = new UserFileDataSource("data", "loginstaff.csv");
        userList = userData.getUserData();
        if (usernameTextFieldStaffPage.getText().equals("") || passwordPasswordFieldStaffPage.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Username or Password");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        } else {
            if (userList.login(usernameTextFieldStaffPage.getText(), passwordPasswordFieldStaffPage.getText())) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                userList.getCurrentAccount().setTime(formatter.format(date));
                userData.setUserList(userList);
                Button enterStaff = (Button) event.getSource();
                Stage stage = (Stage) enterStaff.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

                stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                StaffCenter entStaff = loader.getController();
                entStaff.setUser(userList);
                stage.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong Username or Password");
                alert.setHeaderText(null);
                alert.setContentText("Please enter correct information.");
                alert.showAndWait();
            }
            usernameTextFieldStaffPage.clear();
            passwordPasswordFieldStaffPage.clear();
        }
    }
}
