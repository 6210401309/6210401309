package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;

public class AdminCenter {
    private User user;
    @FXML
    Button backBtnAdmin,logoutBtnAdmin,changePassBtnAdmin,checkLoginAdminBtn,createStaffBtnAdmin;
    @FXML
    Label nameLabelAdmin;

    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelAdmin.setText(user.getCurrentAccount().getName());
            }
        });
    }
    @FXML
    public void logoutBtnAdminOnAction(ActionEvent event) throws IOException {
        Button logoutAdmin = (Button) event.getSource();
        Stage stage = (Stage) logoutAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutAd = loader.getController();
        stage.show();
    }
    public void backBtnAdminOnAction(ActionEvent event) throws IOException {
        Button backAdmin = (Button) event.getSource();
        Stage stage = (Stage) backAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminlogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminLogin backAd = loader.getController();
        stage.show();
    }
    public void changePassBtnAdminOnAction(ActionEvent event) throws IOException {
        Button changePasswordAdmin = (Button) event.getSource();
        Stage stage = (Stage) changePasswordAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminchangepassword.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminChangePassword changePassAd = loader.getController();
        changePassAd.setUser(user);
        stage.show();
    }

    public void checkLoginAdminBtnOnAction (ActionEvent event) throws IOException {
        Button checkLoginAdmin = (Button) event.getSource();
        Stage stage = (Stage) checkLoginAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincheckstafflogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminCheckStaffLogin cheackLoginAd = loader.getController();
        cheackLoginAd.setUser(user);
        stage.show();
    }
    public void createStaffBtnAdminOnAction (ActionEvent event) throws IOException {
        Button createStaffAdmin = (Button) event.getSource();
        Stage stage = (Stage) createStaffAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincreatestaff.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminCreateStaff createStaffAd = loader.getController();
        createStaffAd.setUser(user);
        stage.show();
    }
    public void setUser(User user) {
        this.user = user;
    }
}
