package controller;

import javafx.scene.control.*;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;
import java.sql.SQLOutput;

public class StaffChangePassword {
    @FXML
    Button backBtnChangeStaffPage,logoutBtnChangeStaffPage,confirmBtnChangeStaff;
    @FXML
    PasswordField oldPasswordStaff,passwordChangeStaff,passwordAgainChangeStaff;
    @FXML
    Label nameLabelStaff;
    private User user;
    public void setUser(User user) {
        this.user = user;
    }

    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });
    }

    @FXML
    public void backBtnChangeStaffPageOnAction(ActionEvent event) throws IOException {
        Button backChangeStaff = (Button) event.getSource();
        Stage stage = (Stage) backChangeStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backChangeSf = loader.getController();
        backChangeSf.setUser(user);
        stage.show();

    }

    @FXML
    public void logoutBtnChangeStaffPageOnAction(ActionEvent event) throws IOException {
        Button logoutChangeStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutChangeStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutChangeSf = loader.getController();
        stage.show();
    }
    @FXML
    public void confirmBtnChangeStaffOnAction(ActionEvent event) throws IOException {
        if (passwordChangeStaff.getText().equals("") || oldPasswordStaff.getText().equals("") || passwordAgainChangeStaff.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Missing Information");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        } else {
            if (user.getCurrentAccount().getPassword().equals(oldPasswordStaff.getText())) {
                if (passwordChangeStaff.getText().equals(passwordAgainChangeStaff.getText())) {
                    user.getCurrentAccount().setPassword(passwordChangeStaff.getText());
                    UserFileDataSource userData = new UserFileDataSource("data", "loginstaff.csv");
                    userData.setUserList(user);
                    Button confirmChangePassStaff = (Button) event.getSource();
                    Stage stage = (Stage) confirmChangePassStaff.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/stafflogin.fxml"));

                    stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                    StaffLogin confirmChangeSf = loader.getController();
                    stage.show();
                    Alert alert = new Alert(Alert.AlertType.NONE,"Change Staff Password Done.", ButtonType.OK);
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong Password.");
                    alert.setHeaderText(null);
                    alert.setContentText("Please Enter the same password.");
                    alert.showAndWait();
                    passwordChangeStaff.clear();
                    passwordAgainChangeStaff.clear();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Wrong Password.");
                alert.setHeaderText(null);
                alert.setContentText("Please Enter the correct old Password.");
                alert.showAndWait();
                oldPasswordStaff.clear();
                passwordChangeStaff.clear();
                passwordAgainChangeStaff.clear();
            }
        }
    }
}
