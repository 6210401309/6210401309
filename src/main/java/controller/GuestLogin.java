package controller;

import javafx.scene.control.*;
import model.User;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class GuestLogin {
    UserFileDataSource userData;
    User userList;
    private User user;
    @FXML
    Button homeBtnGuest,enterBtnGuest;
    @FXML
    TextField usernameGuestField;
    @FXML
    PasswordField passwordGuestField;
    public void setUser(User user) {
        this.user = user;
    }
    @FXML
    public void homeBtnGuestOnAction(ActionEvent event) throws IOException {
        Button backHomeGuest = (Button) event.getSource();
        Stage stage = (Stage) backHomeGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome bhGu = loader.getController();
        stage.show();
    }
    @FXML public void initialize(){
        userData = new UserFileDataSource("data","loginguest.csv");
        userList= userData.getUserData();
    }
    public void enterBtnGuestOnAction(ActionEvent event) throws IOException {
        if(usernameGuestField.getText().equals("")|| passwordGuestField.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Username or Password");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        }
        else {
            if (userList.login(usernameGuestField.getText(), passwordGuestField.getText())) {
                Button enterGuest = (Button) event.getSource();
                Stage stage = (Stage) enterGuest.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestcenter.fxml"));

                stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                GuestCenter enterGu = loader.getController();
                enterGu.setUser(userList);
                stage.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong Username or Password");
                alert.setHeaderText(null);
                alert.setContentText("Please enter correct information.");
                alert.showAndWait();
            }
        }
        usernameGuestField.clear();
        passwordGuestField.clear();
    }

}
