package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;

public class GuestCenter {
    @FXML
    Button backBtnGuest,logoutBtnGuest,checkMailBtnGuest,changePasswordBtnGuest;
    @FXML
    Label nameLabelGuest;
    private User user;
    public void setUser(User user) {
        this.user = user;
    }
    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelGuest.setText(user.getCurrentAccount().getName());
            }
        });
    }
    @FXML
    public void logoutBtnGuestOnAction(ActionEvent event) throws IOException {
        Button logoutGuest = (Button) event.getSource();
        Stage stage = (Stage) logoutGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        ProducerPage logoutGu = loader.getController();
        stage.show();
    }
    @FXML
    public void backBtnGuestOnAction(ActionEvent event) throws IOException {
        Button backGuest = (Button) event.getSource();
        Stage stage = (Stage) backGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestlogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestLogin backGu = loader.getController();
        stage.show();
    }

    @FXML
    public void checkMailBtnGuestOnAction(ActionEvent event) throws IOException {
        Button checkMailGuest = (Button) event.getSource();
        Stage stage = (Stage) checkMailGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestcheckmail.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestCheckMail checkMailGu = loader.getController();
        checkMailGu.setUser(user);
        stage.show();
    }
    @FXML
    public void changePasswordBtnGuestOnAction(ActionEvent event) throws IOException {
        Button changePasswordGuest = (Button) event.getSource();
        Stage stage = (Stage) changePasswordGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestchangepassword.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestChangePassword changePasswordGu = loader.getController();
        changePasswordGu.setUser(user);
        stage.show();
    }
}
