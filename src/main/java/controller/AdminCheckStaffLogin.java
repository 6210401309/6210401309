package controller;

import service.StringConfiguration;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class AdminCheckStaffLogin {
    private UserFileDataSource userData;
    private ObservableList<User> userObservarebleList;
    private User userList;
    private User user;
    @FXML
    Button backBtnAdminCheck,logoutBtnAdminCheck;
    @FXML
    TableView checkLoginTableViewAdmin;
    @FXML
    Label nameLabelAdmin;

    @FXML
    public void initialize() throws ParseException {
        userData = new UserFileDataSource("data","loginstaff.csv");
        userList = userData.getUserData();
        showMailData();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelAdmin.setText(user.getCurrentAccount().getName());
            }
        });
    }

    @FXML
    public void logoutBtnAdminCheckOnAction(ActionEvent event) throws IOException {
        Button logoutAdminFourth = (Button) event.getSource();
        Stage stage = (Stage) logoutAdminFourth.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutAdFourth = loader.getController();
        stage.show();
    }
    public void backBtnAdminCheckOnAction(ActionEvent event) throws IOException {
        Button backAdminFourth = (Button) event.getSource();
        Stage stage = (Stage) backAdminFourth.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminCenter backFourthAd = loader.getController();
        backFourthAd.setUser(user);
        stage.show();
    }
    private void showMailData() throws ParseException {
        for(int i = 0; i<userList.getUserBox().size(); i++){
            int min = i;
            for (int j = i + 1; j < userList.getUserBox().size(); j++){
                if(userList.getUserBox().get(j).compareTime(userList.getUserBox().get(min))==1){
                    min = j;
                }
            }
            User swap = userList.getUserBox().get(i);
            userList.getUserBox().set(i,userList.getUserBox().get(min));
            userList.getUserBox().set(min,swap);
        }
        userObservarebleList = FXCollections.observableArrayList(userList.getUserBox());
        checkLoginTableViewAdmin.setItems(userObservarebleList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Name", "field:name"));
        configs.add(new StringConfiguration("title:Username", "field:username"));
        configs.add(new StringConfiguration("title:Time ", "field:time"));


        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            checkLoginTableViewAdmin.getColumns().add(col);
        }
    }
    public void setUser(User user) {
        this.user = user;
    }
}
