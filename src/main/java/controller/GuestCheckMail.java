package controller;

import model.Room;
import service.MailFileDataSource;
import service.ReceiveFileDataSource;
import service.StringConfiguration;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.User;
import model.Mailbox;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class GuestCheckMail {
    private MailFileDataSource mailData;
    private ObservableList<Mailbox> mailObservarebleList;
    private Mailbox mailList;
    private User user;
    private ReceiveFileDataSource receiveData;
    private ObservableList<Mailbox> receiveObservarebleList;
    private Mailbox receiveList;

    @FXML
    TableView mailBoxGuestTableView,receiveMailGuestTableView;
    @FXML
    Button logoutBtnGuestThirdPage,backBtnGuestThirdPage;
    @FXML
    Label nameLabelGuest;
    public void setUser(User user) {
        this.user = user;
    }
    @FXML
    public void initialize() throws ParseException {
        mailData = new MailFileDataSource("data","mailbox.csv");
        mailList = mailData.getMailData();
        receiveData = new ReceiveFileDataSource("data","receivemail.csv");
        receiveList = receiveData.getReceiveData();
        showMailData();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelGuest.setText(user.getCurrentAccount().getName());
            }
        });
    }

    private void showMailData() throws ParseException {
        for (int i = 0; i < mailList.getBox().size(); i++) {
            int min = i;
            for (int j = i + 1; j < mailList.getBox().size(); j++) {
                if (mailList.getBox().get(j).compareTime(mailList.getBox().get(min)) == 1) {
                    min = j;
                }
            }
            Mailbox swap = mailList.getBox().get(i);
            mailList.getBox().set(i, mailList.getBox().get(min));
            mailList.getBox().set(min, swap);

        }
        mailObservarebleList = FXCollections.observableArrayList(mailList.getBox());
        mailBoxGuestTableView.setItems(mailObservarebleList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Type", "field:type"));
        configs.add(new StringConfiguration("title:Recipient", "field: recipient"));
        configs.add(new StringConfiguration("title:Room", "field:numRoom"));
        configs.add(new StringConfiguration("title:Deliver", "field:deliverer"));
        configs.add(new StringConfiguration("title:Size", "field:size"));
        configs.add(new StringConfiguration("title:Staff Add", "field:nameStaffAddMail"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        configs.add(new StringConfiguration("title:Service", "field:service"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackingNumber"));
        configs.add(new StringConfiguration("title:Add Time ", "field:time"));


        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            mailBoxGuestTableView.getColumns().add(col);
        }
        receiveObservarebleList = FXCollections.observableArrayList(receiveList.getBox());
        receiveMailGuestTableView.setItems(receiveObservarebleList);

        configs.add(new StringConfiguration("title:Recipient name ", "field:nameGuest"));
        configs.add(new StringConfiguration("title:Staff receive", "field:nameStaffReceiveMail"));
        configs.add(new StringConfiguration("title:Recipient Time ", "field:receiveTime"));

        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            receiveMailGuestTableView.getColumns().add(col);
        }
    }

    @FXML
    public void logoutBtnGuestThirdPageOnAction(ActionEvent event) throws IOException {
        Button logoutGuest = (Button) event.getSource();
        Stage stage = (Stage) logoutGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutGu = loader.getController();
        stage.show();
    }

    @FXML
    public void backBtnGuestThirdPageOnAction(ActionEvent event) throws IOException {
        Button backGuest = (Button) event.getSource();
        Stage stage = (Stage) backGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestCenter backGu = loader.getController();
        backGu.setUser(user);
        stage.show();
    }


}
