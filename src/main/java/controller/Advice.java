package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;

public class Advice {
 @FXML
 TextArea adviceTextArea;
 @FXML
 Button backBtnAdvice;
 @FXML public void initialize(){
   adviceTextArea.setText("แนะนำการใช้งาน ของ Z Condo เบื้องต้น\n   โปรแกรมจะมีผู้ใช้หลักๆ 3 ประเภทคือ \n    Admin คือ สามารถสร้างStaff และดูการเข้าระบบของstaff ได้\n    Staff คือ เจ้าหน้าที่ส่วนกลาง" +
           "สามารถจัดการกับข้อมูลห้อง ผู้เข้าพัก และจัดการกับจดหมายต่างๆ\n    Guest คือ ผู้เข้าพัก สามารถดูจดหมายที่รับมาแล้วและอยู่กับเจ้าหน้าที่ส่วนกลางได้\n และทุกๆผู้ใช้งานสามารถเปลี่ยนรหัสผ่านได้ \n *****การกรอกข้อมูลควรกรอกให้ครบถ้วน(ที่จำเป็น)" +
           "และกรอกข้อมูลให้สมเหตุสมผล*****");
 }
    @FXML
    public void backBtnAdviceOnAction(ActionEvent event) throws IOException {
        Button backHomeAdvice = (Button) event.getSource();
        Stage stage = (Stage) backHomeAdvice.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome backAdvice = loader.getController();
        stage.show();
    }

}


