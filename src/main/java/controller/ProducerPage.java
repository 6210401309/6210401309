package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class ProducerPage {
    @FXML
    Button homeBtnProduce;
    @FXML
    public void homeBtnProduceOnAction(ActionEvent event) throws IOException {
        System.out.println("Back Home");
        Button backhomeproducer = (Button) event.getSource();
        Stage stage = (Stage) backhomeproducer.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome bhpro = loader.getController();
        stage.show();
    }
}
