package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import service.MailFileDataSource;
import service.RoomFileDataSource;
import service.StringConfiguration;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class StaffAddMail {
    private Mailbox selectMail;
    private MailFileDataSource mailData;
    private ObservableList<Mailbox> mailObservarebleList;
    private Mailbox mailList;
    private RoomFileDataSource roomData;
    private Room roomList;
    @FXML
    TextField typeMailTextField, recipientTextField, nameDeliverTextField,
            sizeTextField, importantTextField, serviceTextField, trackingNumberTextField;
    @FXML
    Button backBtnStaff, logoutBtnStaff;
    @FXML
    TableView<Mailbox> mailboxStaffTableView;
    @FXML
    Label nameLabelMailBox;
    @FXML
    ComboBox <String> numRoomComboBox;
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public void initialize() throws ParseException {
        roomData = new RoomFileDataSource("data", "room.csv");
        roomList = roomData.getRoomData();
        roomData.setRoomList(roomList);
        mailData = new MailFileDataSource("data", "mailbox.csv");
        mailList = mailData.getMailData();
        mailData.setMailList(mailList);
        showMailData();
        for(Room room:roomList.getRooms()){
            numRoomComboBox.getItems().addAll(String.valueOf(room.getNumRoom()));
        }
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelMailBox.setText(user.getCurrentAccount().getName());
            }
        });
        typeMailTextField.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue.equals("Letter") || newValue.equals("letter")) {
                importantTextField.setDisable(true);
                trackingNumberTextField.setDisable(true);
                serviceTextField.setDisable(true);
            } else if (newValue.equals("Document") || newValue.equals("document")) {
                importantTextField.setDisable(false);
                trackingNumberTextField.setDisable(true);
                serviceTextField.setDisable(true);
            } else if (newValue.equals("Parcel") || newValue.equals("parcel")) {
                importantTextField.setDisable(true);
                trackingNumberTextField.setDisable(false);
                serviceTextField.setDisable(false);
            } else {
                importantTextField.setDisable(false);
                trackingNumberTextField.setDisable(false);
                serviceTextField.setDisable(false);
            }
        }));

        mailboxStaffTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue != null) {
                showSelectMail(newValue);
            }
        });
    }

    private void showSelectMail(Mailbox mail) {
        selectMail = mail;
        typeMailTextField.clear();
        recipientTextField.clear();
        trackingNumberTextField.clear();
        numRoomComboBox.setValue("Room");
        nameDeliverTextField.clear();
        sizeTextField.clear();
        importantTextField.clear();
        serviceTextField.clear();
        trackingNumberTextField.clear();
        typeMailTextField.setText(mail.getType());
        recipientTextField.setText(mail.getRecipient());
        numRoomComboBox.setValue(String.valueOf(mail.getNumRoom()));
        nameDeliverTextField.setText(mail.getDeliverer());
        sizeTextField.setText(mail.getSize());
        if (mail.getType().equals("Parcel")) {
            Parcel parcel = (Parcel) mail;
            trackingNumberTextField.setText(parcel.getTrackingNumber());
            serviceTextField.setText(parcel.getService());
        } else if (mail.getType().equals("Document")) {
            Document document = (Document) mail;
            importantTextField.setText(document.getImportant());
        }
    }


    @FXML
    public void addMailBoxOnAction(ActionEvent event) throws ParseException {
        if (typeMailTextField.getText().equals("") || recipientTextField.getText().equals("") || numRoomComboBox.getValue() == null || nameDeliverTextField.getText().equals("") || sizeTextField.getText().equals("")
                || sizeTextField.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Information");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        }
        else {
            if (typeMailTextField.getText().equals("Letter") || typeMailTextField.getText().equals("letter")) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                Letter newLetter = new Letter(typeMailTextField.getText(), recipientTextField.getText(), Integer.parseInt(numRoomComboBox.getValue()), nameDeliverTextField.getText()
                        , sizeTextField.getText(), user.getCurrentAccount().getName());
                newLetter.setTime(formatter.format(date));
                Alert alert = new Alert(Alert.AlertType.NONE,"Add Letter already.",ButtonType.OK);
                alert.showAndWait();
                mailList.add(newLetter);
                mailData.setMailList(mailList);
                typeMailTextField.clear();
                recipientTextField.clear();
                numRoomComboBox.setValue("Room");
                nameDeliverTextField.clear();
                sizeTextField.clear();
                importantTextField.clear();
                serviceTextField.clear();
                trackingNumberTextField.clear();

            } else if (typeMailTextField.getText().equals("Document") || typeMailTextField.getText().equals("document")) {
                if (importantTextField.getText().equals("")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Wrong Information");
                    alert.setHeaderText(null);
                    alert.setContentText("Please complete all information!!");
                    alert.showAndWait();
                }
                else{
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    Document newDocument = new Document(typeMailTextField.getText(), recipientTextField.getText(), Integer.parseInt(numRoomComboBox.getValue()), nameDeliverTextField.getText()
                            , sizeTextField.getText(), user.getCurrentAccount().getName(), importantTextField.getText());
                    newDocument.setTime(formatter.format(date));
                    Alert alert = new Alert(Alert.AlertType.NONE,"Add Document already.",ButtonType.OK);
                    alert.showAndWait();
                    mailList.add(newDocument);
                    mailData.setMailList(mailList);
                    typeMailTextField.clear();
                    recipientTextField.clear();
                    numRoomComboBox.setValue("Room");
                    nameDeliverTextField.clear();
                    sizeTextField.clear();
                    importantTextField.clear();
                    serviceTextField.clear();
                    trackingNumberTextField.clear();
                }
            }
            else if (typeMailTextField.getText().equals("Parcel") || typeMailTextField.getText().equals("parcel")) {
                if (serviceTextField.getText().equals("") || trackingNumberTextField.getText().equals("")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Wrong Information");
                    alert.setHeaderText(null);
                    alert.setContentText("Please complete all information!!");
                    alert.showAndWait();
                }
                else{
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    Parcel newParcel = new Parcel(typeMailTextField.getText(), recipientTextField.getText(), Integer.parseInt(numRoomComboBox.getValue()), nameDeliverTextField.getText()
                            , sizeTextField.getText(), user.getCurrentAccount().getName(), serviceTextField.getText(), trackingNumberTextField.getText());
                    newParcel.setTime(formatter.format(date));
                    Alert alert = new Alert(Alert.AlertType.NONE,"Add Parcel already.",ButtonType.OK);
                    alert.showAndWait();
                    mailList.add(newParcel);
                    mailData.setMailList(mailList);
                    typeMailTextField.clear();
                    recipientTextField.clear();
                    numRoomComboBox.setValue("Room");
                    nameDeliverTextField.clear();
                    sizeTextField.clear();
                    importantTextField.clear();
                    serviceTextField.clear();
                    trackingNumberTextField.clear();
                }
            }
            else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong Type Mail");
                alert.setHeaderText("Please Enter the correct type mail.");
                alert.setContentText("Enter : Letter or Document or Parcel");
                alert.showAndWait();
            }
            mailData.setMailList(mailList);
            mailboxStaffTableView.getColumns().clear();
            showMailData();
        }
    }

    @FXML
    public void backBtnStaffOnAction(ActionEvent event) throws IOException {
        Button backStaff = (Button) event.getSource();
        Stage stage = (Stage) backStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backSf = loader.getController();
        backSf.setUser(user);
        stage.show();
    }

    @FXML
    public void logoutBtnStaffOnAction(ActionEvent event) throws IOException {
        Button logoutStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutSf = loader.getController();
        stage.show();
    }

    private void showMailData() throws ParseException {
        for (int i = 0; i < mailList.getBox().size(); i++) {
            int min = i;
            for (int j = i + 1; j < mailList.getBox().size(); j++) {
                if (mailList.getBox().get(j).compareTime(mailList.getBox().get(min)) == 1) {
                    min = j;
                }
            }
            Mailbox swap = mailList.getBox().get(i);
            mailList.getBox().set(i, mailList.getBox().get(min));
            mailList.getBox().set(min, swap);
        }
        mailObservarebleList = FXCollections.observableArrayList(mailList.getBox());
        mailboxStaffTableView.setItems(mailObservarebleList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Type", "field:type"));
        configs.add(new StringConfiguration("title:Recipient", "field: recipient"));
        configs.add(new StringConfiguration("title:Room", "field:numRoom"));
        configs.add(new StringConfiguration("title:Deliver", "field:deliverer"));
        configs.add(new StringConfiguration("title:Size", "field:size"));
        configs.add(new StringConfiguration("title:Staff Add", "field:nameStaffAddMail"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        configs.add(new StringConfiguration("title:Service", "field:service"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackingNumber"));
        configs.add(new StringConfiguration("title:Add Time ", "field:time"));


        for (StringConfiguration conf : configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            mailboxStaffTableView.getColumns().add(col);
        }
    }
}
