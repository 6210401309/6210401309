package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.User;
import service.UserFileDataSource;

import java.io.IOException;

public class StaffCenter {
    UserFileDataSource userData;
    private User userList;
    @FXML
    Button backBtnStaffSecondPage,logoutBtnStaffSecondPage,changePasswordBtnStaffPage,addMailboxBtnStaffPage,searchRoomBtnStaffPage,createRoomBtnStaffPage,receiveMailBtnStaffPage,searchMailBtnStaff;
    @FXML
    Label nameLabelStaff;
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });
    }
    @FXML
    public void backBtnStaffSecondPageOnAction(ActionEvent event) throws IOException {
        Button backCenterStaff = (Button) event.getSource();
        Stage stage = (Stage) backCenterStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/stafflogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffLogin bkCenterSf = loader.getController();
        stage.show();
    }

    @FXML
    public void logoutBtnStaffSecondPageOnAction(ActionEvent event) throws IOException {
        Button logoutCenterStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutCenterStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome lgSecondSf = loader.getController();
        stage.show();
    }

    @FXML
    public void changePasswordBtnStaffPageOnAction(ActionEvent event) throws IOException {
        Button changePasswordStaff = (Button) event.getSource();
        Stage stage = (Stage) changePasswordStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffchangepassword.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffChangePassword staffChangeSf = loader.getController();
        staffChangeSf.setUser(user);
        stage.show();
    }

    @FXML
    public void addMailboxBtnStaffPageOnAction(ActionEvent event) throws IOException {
        Button addMailStaff = (Button) event.getSource();
        Stage stage = (Stage) addMailStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffaddmail.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffAddMail addMailSf = loader.getController();
        addMailSf.setUser(user);
        stage.show();
    }

    @FXML
    public void searchRoomBtnStaffPageOnAction(ActionEvent event) throws IOException {
        Button searchRoomStaff = (Button) event.getSource();
        Stage stage = (Stage) searchRoomStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffsearchroomandedit.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffSearchRoomAndEdit shRoomSf = loader.getController();
        shRoomSf.setUser(user);
        stage.show();
    }
    @FXML
    public void createRoomBtnStaffPageOnAction(ActionEvent event) throws IOException {
        Button createRoomStaff = (Button) event.getSource();
        Stage stage = (Stage) createRoomStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcreateroomandguest.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCreateRoomAndGuest createRoomSf = loader.getController();
        createRoomSf.setUser(user);
        stage.show();
    }
    @FXML
    public void receiveMailBtnStaffPageOnAction(ActionEvent event) throws IOException {
        Button receiveMailStaff = (Button) event.getSource();
        Stage stage = (Stage) receiveMailStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffreceivemail.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffReceiveMail receiveMailSf = loader.getController();
        receiveMailSf.setUser(user);
        stage.show();
    }
    @FXML
    public void searchMailBtnStaffOnAction(ActionEvent event) throws IOException {
        Button searchMailStaff = (Button) event.getSource();
        Stage stage = (Stage) searchMailStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffsearchmail.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffSearchMail searchMailSf = loader.getController();
        searchMailSf.setUser(user);
        stage.show();
    }
}
