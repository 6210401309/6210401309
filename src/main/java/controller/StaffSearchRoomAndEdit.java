package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Extraroom;
import model.Room;
import model.User;
import service.RoomFileDataSource;
import service.StringConfiguration;

import java.io.IOException;
import java.util.ArrayList;

public class StaffSearchRoomAndEdit {
    private User user;
    @FXML
    Button backBtnSearchRoomPage,logoutBtnSearchRoomPage,doneBtnSearchRoomPage,editBtnSearchRoomPage;
    @FXML
    TableView<Room> searchRoomTableViewStaff;
    @FXML
    Label nameLabelStaff;
    private Room selectRoom;
    @FXML
    TextField searchRoomTextFieldStaffPage,firstEditNameTextFieldStaff,secondEditNameTextFieldStaff,thirdEditNameTextFieldStaff;
    private RoomFileDataSource roomData;
    private ObservableList<Room> roomObservarebleList;
    private Room roomList;
    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                showMailData("");
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });


        roomData = new RoomFileDataSource("data", "room.csv");
        roomList = roomData.getRoomData();
        roomData.setRoomList(roomList);
        searchRoomTableViewStaff.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue != null) {
                showSelectMail(newValue);
            }
        });
    }

    private void showSelectMail(Room room) {
        selectRoom = room;
        firstEditNameTextFieldStaff.clear();
        secondEditNameTextFieldStaff.clear();
        thirdEditNameTextFieldStaff.clear();
        firstEditNameTextFieldStaff.setText(room.getName());
        secondEditNameTextFieldStaff.setDisable(true);
        thirdEditNameTextFieldStaff.setDisable(true);
        if (room.getTypeRoom().equals("Extra")) {
            Extraroom extra = (Extraroom) room;
            secondEditNameTextFieldStaff.setText(extra.getSecondName());
            thirdEditNameTextFieldStaff.setText(extra.getThirdName());
            secondEditNameTextFieldStaff.setDisable(false);
            thirdEditNameTextFieldStaff.setDisable(false);
        }
    }
    @FXML
    public void backBtnSearchRoomPageOnAction(ActionEvent event) throws IOException {
        Button backSearchRoomStaff = (Button) event.getSource();
        Stage stage = (Stage) backSearchRoomStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backSearchRoomSf = loader.getController();
        backSearchRoomSf.setUser(user);
        stage.show();
    }
    @FXML
    public void editBtnSearchRoomPageOnAction(ActionEvent event) throws IOException {
        if(firstEditNameTextFieldStaff.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Name");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter First name.");
            alert.showAndWait();
        }
        else {
            selectRoom.setName(firstEditNameTextFieldStaff.getText());
            if (selectRoom.getTypeRoom().equals("Extra")) {
                Extraroom extra = (Extraroom) selectRoom;
                if (secondEditNameTextFieldStaff.getText().equals("")) {
                    secondEditNameTextFieldStaff.setText("-");
                }
                if (thirdEditNameTextFieldStaff.getText().equals("")) {
                    thirdEditNameTextFieldStaff.setText("-");
                }

                extra.setSecondName(secondEditNameTextFieldStaff.getText());
                extra.setThirdName(thirdEditNameTextFieldStaff.getText());
            }
            Alert alert = new Alert(Alert.AlertType.NONE,"Edit Done.",ButtonType.OK);
            alert.showAndWait();
        }
        roomData.setRoomList(roomList);
        showMailData("");
        firstEditNameTextFieldStaff.clear();
        secondEditNameTextFieldStaff.clear();
        thirdEditNameTextFieldStaff.clear();

    }
    public void logoutBtnSearchRoomPageOnAction(ActionEvent event) throws IOException {
        Button logoutSearchRoomStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutSearchRoomStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutSearchRoomSf = loader.getController();
        stage.show();
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void doneBtnSearchRoomPageOnAction() {
        if(searchRoomTextFieldStaffPage.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Search");
            alert.setHeaderText(null);
            alert.setContentText("Please Enter name!!");
            alert.showAndWait();
        }
        else {
            searchRoomTableViewStaff.getColumns().clear();
            showMailData(searchRoomTextFieldStaffPage.getText());
        }
        searchRoomTextFieldStaffPage.clear();
    }
    private void showMailData(String search) {
        searchRoomTableViewStaff.getColumns().clear();
        Room rooms = new Room();
        for(Room a:roomList.getRooms()){
            if(a.getName().toLowerCase().contains(search.toLowerCase())){
                rooms.add(a);
            }
        }
        if(search.equals("Enter  owner name :")||search.equals("")) {
            roomObservarebleList = FXCollections.observableArrayList(roomList.getRooms());
        }
        else{
            roomObservarebleList = FXCollections.observableArrayList(rooms.getRooms());
        }
        searchRoomTableViewStaff.setItems(roomObservarebleList);
            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Type", "field:typeRoom"));
            configs.add(new StringConfiguration("title:Building", "field:building"));
            configs.add(new StringConfiguration("title:Level", "field:level"));
            configs.add(new StringConfiguration("title:Room", "field:numRoom"));
            configs.add(new StringConfiguration("title:Owner", "field:name"));
            configs.add(new StringConfiguration("title:Name ", "field:secondName"));
            configs.add(new StringConfiguration("title:Name ", "field:thirdName"));

            for (StringConfiguration conf: configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                searchRoomTableViewStaff.getColumns().add(col);
            }
        }
}
