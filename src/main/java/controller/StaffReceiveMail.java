package controller;

import model.*;
import service.MailFileDataSource;
import service.ReceiveFileDataSource;
import service.StringConfiguration;
import javafx.scene.control.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StaffReceiveMail {
    private MailFileDataSource mailData;
    private ObservableList<Mailbox> mailObservarebleList;
    private Mailbox mailList;
    private User user;
    private Mailbox selectmail;
    private ReceiveFileDataSource receiveData;
    private ObservableList<Mailbox> receiveObservarebleList;
    private Mailbox receiveList;
    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    Button logoutBtnReceive, backBtnReceive, receiveMaliStaff;
    @FXML
    Label nameLabelStaff;
    @FXML
    TableView<Mailbox> mailBoxTableViewStaff, receiveMailTableViewStaff;
    @FXML
    TextField nameGuestTextField;

    @FXML
    public void initialize() throws ParseException {
        mailData = new MailFileDataSource("data", "mailbox.csv");
        mailList = mailData.getMailData();
        receiveData = new ReceiveFileDataSource("data","receivemail.csv");
        receiveList = receiveData.getReceiveData();
        showMailData();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });
        mailBoxTableViewStaff.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue != null) {
                selectmail = newValue;
            }
        });
    }

    @FXML
    public void backBtnReceiveOnAction(ActionEvent event) throws IOException {
        Button backReceiveStaff = (Button) event.getSource();
        Stage stage = (Stage) backReceiveStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backReceiveSf = loader.getController();
        backReceiveSf.setUser(user);
        stage.show();

    }

    @FXML
    public void logoutBtnReceiveOnAction(ActionEvent event) throws IOException {
        Button logoutReceiveStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutReceiveStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutChangeSf = loader.getController();
        stage.show();
    }

    public void receiveMaliStaffOnAction() throws ParseException {
        ReceiveFileDataSource receiveFileDataSource = new ReceiveFileDataSource("data", "receivemail.csv");
        Mailbox receiveMail = receiveFileDataSource.getReceiveData();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        if(nameGuestTextField.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Name");
            alert.setHeaderText(null);
            alert.setContentText("Please complete information!!");
            alert.showAndWait();
        }
        else {
            if (selectmail.getType().equals("Letter")) {
                ReceiveLetter letter = new ReceiveLetter(selectmail, nameGuestTextField.getText(), user.getCurrentAccount().getName(), formatter.format(date));
                receiveMail.add(letter);
                Alert alert = new Alert(Alert.AlertType.NONE,"Receive Letter Done.",ButtonType.OK);
                alert.showAndWait();
            } else if (selectmail.getType().equals("Document")) {
                Document doc = (Document) selectmail;
                ReceiveDocument document = new ReceiveDocument(doc, nameGuestTextField.getText(), user.getCurrentAccount().getName(), formatter.format(date));
                receiveMail.add(document);
                Alert alert = new Alert(Alert.AlertType.NONE,"Receive Document Done.",ButtonType.OK);
                alert.showAndWait();
            } else if (selectmail.getType().equals("Parcel")) {
                Parcel par = (Parcel) selectmail;
                ReceiveParcel parcel = new ReceiveParcel(par, nameGuestTextField.getText(), user.getCurrentAccount().getName(), formatter.format(date));
                receiveMail.add(parcel);
                Alert alert = new Alert(Alert.AlertType.NONE,"Receive Parcel Done.",ButtonType.OK);
                alert.showAndWait();
            }
            receiveFileDataSource.setReceiveList(receiveMail);
            mailBoxTableViewStaff.getColumns().clear();
            receiveMailTableViewStaff.getColumns().clear();
            nameGuestTextField.clear();
            mailList.getBox().remove(selectmail);
            mailData.setMailList(mailList);
            showMailData();
        }
    }

    private void showMailData() throws ParseException {
        for (int i = 0; i < mailList.getBox().size(); i++) {
            int min = i;
            for (int j = i + 1; j < mailList.getBox().size(); j++) {
                if (mailList.getBox().get(j).compareTime(mailList.getBox().get(min)) == 1) {
                    min = j;
                }
            }
            Mailbox swap = mailList.getBox().get(i);
            mailList.getBox().set(i, mailList.getBox().get(min));
            mailList.getBox().set(min, swap);
        }
            mailObservarebleList = FXCollections.observableArrayList(mailList.getBox());
            mailBoxTableViewStaff.setItems(mailObservarebleList);

            ArrayList<StringConfiguration> configs = new ArrayList<>();
            configs.add(new StringConfiguration("title:Type", "field:type"));
            configs.add(new StringConfiguration("title:Recipient", "field: recipient"));
            configs.add(new StringConfiguration("title:Room", "field:numRoom"));
            configs.add(new StringConfiguration("title:Deliver", "field:deliverer"));
            configs.add(new StringConfiguration("title:Size", "field:size"));
            configs.add(new StringConfiguration("title:Staff Add", "field:nameStaffAddMail"));
            configs.add(new StringConfiguration("title:Important", "field:important"));
            configs.add(new StringConfiguration("title:Service", "field:service"));
            configs.add(new StringConfiguration("title:Tracking Number", "field:trackingNumber"));
            configs.add(new StringConfiguration("title:Time ", "field:time"));


            for (StringConfiguration conf : configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                mailBoxTableViewStaff.getColumns().add(col);
            }
            receiveObservarebleList = FXCollections.observableArrayList(receiveList.getBox());
            receiveMailTableViewStaff.setItems(receiveObservarebleList);

            configs.add(new StringConfiguration("title:Recipient name ", "field:nameGuest"));
            configs.add(new StringConfiguration("title:Staff receive", "field:nameStaffReceiveMail"));
            configs.add(new StringConfiguration("title:Recipient Time ", "field:receiveTime"));

            for (StringConfiguration conf: configs) {
                TableColumn col = new TableColumn(conf.get("title"));
                col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
                receiveMailTableViewStaff.getColumns().add(col);
            }
            receiveObservarebleList = FXCollections.observableArrayList(receiveList.getBox());
            receiveMailTableViewStaff.setItems(receiveObservarebleList);
        }
    }
