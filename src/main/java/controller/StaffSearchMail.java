package controller;

import service.MailFileDataSource;
import service.StringConfiguration;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.User;
import model.Mailbox;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class StaffSearchMail {
    @FXML
    Button backBtnSearchMailPage,logoutBtnSearchMailPage,doneBtnSearchMailPage;
    @FXML
    TextField searchMailTextFieldStaffPage;
    @FXML
    TableView searchMailTableViewStaff;
    @FXML
    Label nameLabelStaff;
    private User user;
    private MailFileDataSource mailData;
    private ObservableList<Mailbox> mailObservarebleList;
    private Mailbox mailList;
    @FXML public void initialize() throws ParseException {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });
        showMailData("");
    }
    @FXML
    public void backBtnSearchMailPageOnAction(ActionEvent event) throws IOException {
        Button backSearchMailStaff = (Button) event.getSource();
        Stage stage = (Stage) backSearchMailStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backSearchMailSf = loader.getController();
        backSearchMailSf.setUser(user);
        stage.show();
    }
    @FXML
    public void logoutBtnSearchMailOnAction(ActionEvent event) throws IOException {
        Button logoutSearchMailStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutSearchMailStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutSearchMailSf = loader.getController();
        stage.show();
    }
    public void setUser(User user) {
        this.user = user;
    }
    public void doneBtnSearchMailPageOnAction() throws ParseException {
        searchMailTableViewStaff.getColumns().clear();
        showMailData(searchMailTextFieldStaffPage.getText());
    }
    private void showMailData(String search) throws ParseException {
        mailData = new MailFileDataSource("data","mailbox.csv");
        mailList = mailData.getMailData();
        for (int i = 0; i < mailList.getBox().size(); i++) {
            int min = i;
            for (int j = i + 1; j < mailList.getBox().size(); j++) {
                if (mailList.getBox().get(j).compareTime(mailList.getBox().get(min)) == 1) {
                    min = j;
                }
            }
            Mailbox swap = mailList.getBox().get(i);
            mailList.getBox().set(i, mailList.getBox().get(min));
            mailList.getBox().set(min, swap);
        }
        Mailbox mailsbox= new Mailbox();
        for(Mailbox a:mailList.getBox()){
            if(String.valueOf(a.getNumRoom()).contains(search)){
                mailsbox.add(a);
            }
        }
        if(search.equals("Enter Room Number :")||search.equals("")) {
            mailObservarebleList = FXCollections.observableArrayList(mailList.getBox());
        }
        else{
            mailObservarebleList = FXCollections.observableArrayList(mailsbox.getBox());
        }
        searchMailTableViewStaff.setItems(mailObservarebleList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Type", "field:type"));
        configs.add(new StringConfiguration("title:Recipient", "field: recipient"));
        configs.add(new StringConfiguration("title:Room", "field:numRoom"));
        configs.add(new StringConfiguration("title:Deliver", "field:deliverer"));
        configs.add(new StringConfiguration("title:Size", "field:size"));
        configs.add(new StringConfiguration("title:Staff Add", "field:nameStaffAddMail"));
        configs.add(new StringConfiguration("title:Important", "field:important"));
        configs.add(new StringConfiguration("title:Service", "field:service"));
        configs.add(new StringConfiguration("title:Tracking Number", "field:trackingNumber"));
        configs.add(new StringConfiguration("title:Time ", "field:time"));

        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            searchMailTableViewStaff.getColumns().add(col);
        }
    }
}
