package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Extraroom;
import model.Room;
import model.User;
import service.RoomFileDataSource;
import service.StringConfiguration;
import service.UserFileDataSource;

import java.io.IOException;
import java.util.ArrayList;

public class StaffCreateRoomAndGuest {
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    Button backBtnCreateStaffPage, logoutBtnCreateStaffPage, doneBtnCreateRoomStaff;
    @FXML
    MenuButton typeRoomMenuBtnStaff, buildingMenuBtnStaff, levelMenuBtnStaff,roomMenuBtnStaff;
    @FXML
    MenuItem typeRoomNormalStaff, typeRoomExtraStaff, buildingOneStaff, buildingTwoStaff, levelOneStaff, levelTwoStaff, levelThreeStaff, levelFourStaff, levelFiveStaff, levelSixStaff, levelSevenStaff, levelEightStaff,roomOneStaff,roomTwoStaff,roomThreeStaff,roomFourStaff,roomFiveStaff,roomSixStaff,roomSevenStaff,roomEightStaff,roomNineStaff,roomTenStaff;
    @FXML
    TextField nameTextFieldOneCreate,nameTextFieldTwoCreate,nameTextFieldThreeCreate,usernameTextFieldCreate;
    @FXML
    PasswordField passwordTextFieldCreate, passwordAgainTextFieldCreate;
    @FXML
    TableView<Room> createRoomTableViewStaff;
    @FXML
    Label nameLabelStaff;

    private RoomFileDataSource roomData;
    private ObservableList<Room> roomObservarebleList;
    private Room roomList;
    private UserFileDataSource userData;
    private ObservableList<User> userObservableList;
    private ArrayList<Room> rooms;
    private User userList;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelStaff.setText(user.getCurrentAccount().getName());
            }
        });
        roomData = new RoomFileDataSource("data", "room.csv");
        roomList = roomData.getRoomData();
        roomData.setRoomList(roomList);
        showRoomData();
        EventHandler<ActionEvent> eventTypeRoom = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                typeRoomMenuBtnStaff.setText(((MenuItem) e.getSource()).getText());
            }
        };
        typeRoomExtraStaff.setOnAction(eventTypeRoom);
        typeRoomNormalStaff.setOnAction(eventTypeRoom);
        EventHandler<ActionEvent> eventBuild = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                buildingMenuBtnStaff.setText(((MenuItem) e.getSource()).getText());
            }
        };
        buildingOneStaff.setOnAction(eventBuild);
        buildingTwoStaff.setOnAction(eventBuild);
        EventHandler<ActionEvent> eventLevel = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                levelMenuBtnStaff.setText(((MenuItem) e.getSource()).getText());
            }
        };
        levelOneStaff.setOnAction(eventLevel);
        levelTwoStaff.setOnAction(eventLevel);
        levelThreeStaff.setOnAction(eventLevel);
        levelFourStaff.setOnAction(eventLevel);
        levelFiveStaff.setOnAction(eventLevel);
        levelSixStaff.setOnAction(eventLevel);
        levelSevenStaff.setOnAction(eventLevel);
        levelEightStaff.setOnAction(eventLevel);

        EventHandler<ActionEvent> eventRoom = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                roomMenuBtnStaff.setText(((MenuItem) e.getSource()).getText());
            }
        };
        roomOneStaff.setOnAction(eventRoom);
        roomTwoStaff.setOnAction(eventRoom);
        roomThreeStaff.setOnAction(eventRoom);
        roomFourStaff.setOnAction(eventRoom);
        roomFiveStaff.setOnAction(eventRoom);
        roomSixStaff.setOnAction(eventRoom);
        roomSevenStaff.setOnAction(eventRoom);
        roomEightStaff.setOnAction(eventRoom);
        roomNineStaff.setOnAction(eventRoom);
        roomTenStaff.setOnAction(eventRoom);
        typeRoomMenuBtnStaff.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue.equals("Normal") ) {
                nameTextFieldTwoCreate.setDisable(true);
                nameTextFieldThreeCreate.setDisable(true);
            }else {
                nameTextFieldTwoCreate.setDisable(false);
                nameTextFieldThreeCreate.setDisable(false);
            }
        }));
        showRoomData();
    }
    @FXML
    public void doneBtnCreateRoomStaffOnAction(ActionEvent event) {
        userData = new UserFileDataSource("data", "loginguest.csv");
        userList = userData.getUserData();
        //userList.addUser(nameTextFieldOneCreate.getText(), usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText());

        roomData = new RoomFileDataSource("data", "room.csv");
        roomList = roomData.getRoomData();
        if (typeRoomMenuBtnStaff.getText().equals("") || buildingMenuBtnStaff.getText().equals("") || levelMenuBtnStaff.getText().equals("") || roomMenuBtnStaff.getText().equals("")
                || nameTextFieldOneCreate.getText().equals("") || usernameTextFieldCreate.getText().equals("") || passwordTextFieldCreate.getText().equals("") || passwordAgainTextFieldCreate.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Missing Information");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        }
        else {
            if (userList.login(usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Use Another Username or Password");
                alert.setHeaderText(null);
                alert.setContentText("Please use another Username or Password.");
                alert.showAndWait();
            } else {
                if (passwordTextFieldCreate.getText().equals(passwordAgainTextFieldCreate.getText())) {
                    if (roomList.used(Integer.parseInt(buildingMenuBtnStaff.getText()), Integer.parseInt(levelMenuBtnStaff.getText()), Integer.parseInt(buildingMenuBtnStaff.getText() + levelMenuBtnStaff.getText() + roomMenuBtnStaff.getText()))) {
                        if (typeRoomMenuBtnStaff.getText().equals("Normal")) {
                                userList.addUser(nameTextFieldOneCreate.getText(), usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText());
                                roomList.addRoom(typeRoomMenuBtnStaff.getText(), Integer.parseInt(buildingMenuBtnStaff.getText()), Integer.parseInt(levelMenuBtnStaff.getText())
                                        , Integer.parseInt(buildingMenuBtnStaff.getText() + levelMenuBtnStaff.getText() + roomMenuBtnStaff.getText()), nameTextFieldOneCreate.getText(), usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText(), passwordAgainTextFieldCreate.getText());
                                Alert alert = new Alert(Alert.AlertType.NONE, "Create Room And Create Guest Already.", ButtonType.OK);
                                alert.showAndWait();
                        } else if (typeRoomMenuBtnStaff.getText().equals("Extra")) {
                            if (typeRoomMenuBtnStaff.getText().equals("") || buildingMenuBtnStaff.getText().equals("") || levelMenuBtnStaff.getText().equals("") || roomMenuBtnStaff.getText().equals("")
                                    || nameTextFieldOneCreate.getText().equals("") || usernameTextFieldCreate.getText().equals("") || passwordTextFieldCreate.getText().equals("") || passwordAgainTextFieldCreate.getText().equals("")) {

                                Alert alert = new Alert(Alert.AlertType.ERROR);
                                alert.setTitle("Missing Information");
                                alert.setHeaderText(null);
                                alert.setContentText("Please complete all information for Extra room.");
                                alert.showAndWait();
                            } else {
                                if (nameTextFieldTwoCreate.getText().equals("")) {
                                    nameTextFieldTwoCreate.setText("-");
                                }
                                if (nameTextFieldThreeCreate.getText().equals("")) {
                                    nameTextFieldThreeCreate.setText("-");
                                }
                                Extraroom newExtraroom = new Extraroom(typeRoomMenuBtnStaff.getText(), Integer.parseInt(buildingMenuBtnStaff.getText()), Integer.parseInt(levelMenuBtnStaff.getText())
                                        , Integer.parseInt(buildingMenuBtnStaff.getText() + levelMenuBtnStaff.getText() + roomMenuBtnStaff.getText()), nameTextFieldOneCreate.getText(), usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText(), passwordAgainTextFieldCreate.getText(),
                                        nameTextFieldTwoCreate.getText(), nameTextFieldThreeCreate.getText());
                                roomList.add(newExtraroom);
                                userList.addUser(nameTextFieldOneCreate.getText(), usernameTextFieldCreate.getText(), passwordTextFieldCreate.getText());
                                Alert alert = new Alert(Alert.AlertType.NONE, "Create Room And Create Guest Already.", ButtonType.OK);
                                alert.showAndWait();
                            }
                        }
                    }
                    else {
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Use Another Room");
                        alert.setHeaderText(null);
                        alert.setContentText("This room already have owner.");
                        alert.showAndWait();
                    }
                }
                else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong Password.");
                    alert.setHeaderText(null);
                    alert.setContentText("Please Enter the same password.");
                    alert.showAndWait();
                }
            }
        }
        typeRoomMenuBtnStaff.setText("Type Room");
        buildingMenuBtnStaff.setText("Building");
        levelMenuBtnStaff.setText("Level");
        roomMenuBtnStaff.setText("Room");
        nameTextFieldOneCreate.clear();
        nameTextFieldTwoCreate.clear();
        nameTextFieldThreeCreate.clear();
        usernameTextFieldCreate.clear();
        passwordTextFieldCreate.clear();
        passwordAgainTextFieldCreate.clear();
        roomData.setRoomList(roomList);
        userData.setUserList(userList);
        showRoomData();
}
    private void showRoomData() {
        createRoomTableViewStaff.getColumns().clear();
        roomObservarebleList = FXCollections.observableArrayList(roomList.getRooms());
        createRoomTableViewStaff.setItems(roomObservarebleList);

        ArrayList<StringConfiguration> configs = new ArrayList<>();
        configs.add(new StringConfiguration("title:Type", "field:typeRoom"));
        configs.add(new StringConfiguration("title:Building", "field:building"));
        configs.add(new StringConfiguration("title:Level", "field:level"));
        configs.add(new StringConfiguration("title:Room", "field:numRoom"));
        configs.add(new StringConfiguration("title:Owner", "field:name"));
        configs.add(new StringConfiguration("title:Name ", "field:secondName"));
        configs.add(new StringConfiguration("title:Name ", "field:thirdName"));

        for (StringConfiguration conf: configs) {
            TableColumn col = new TableColumn(conf.get("title"));
            col.setCellValueFactory(new PropertyValueFactory<>(conf.get("field")));
            createRoomTableViewStaff.getColumns().add(col);
        }

    }

    @FXML
    public void backBtnCreateStaffPageOnAction(ActionEvent event) throws IOException {
        Button backCreateStaff = (Button) event.getSource();
        Stage stage = (Stage) backCreateStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/staffcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffCenter backCreateSf = loader.getController();
        backCreateSf.setUser(user);
        stage.show();
    }
    @FXML
    public void logoutBtnCreateStaffPageOnAction(ActionEvent event) throws IOException {
        Button logoutCreateStaff = (Button) event.getSource();
        Stage stage = (Stage) logoutCreateStaff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutCreateSf = loader.getController();
        stage.show();
    }
}

