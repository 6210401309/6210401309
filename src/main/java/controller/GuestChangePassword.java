package controller;

import javafx.scene.control.*;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;

public class GuestChangePassword {
    @FXML
    Button logoutBtnChangeGuest,backBtnChangeGuest,confirmBtnChangeGuest;
    @FXML
    Label nameLabelGuest;
    @FXML
    PasswordField oldPasswordGuest,passwordChangeGuest,passwordAgainChangeGuest;
    private User user;
    public void setUser(User user) {
        this.user = user;
    }
    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelGuest.setText(user.getCurrentAccount().getName());
            }
        });
    }
    @FXML
    public void logoutBtnChangeGuestOnAction(ActionEvent event) throws IOException {
        Button logoutChangeGuest = (Button) event.getSource();
        Stage stage = (Stage) logoutChangeGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        ProducerPage logoutChangeGu = loader.getController();
        stage.show();
    }
    @FXML
    public void backBtnChangeGuestOnAction(ActionEvent event) throws IOException {
        Button backChangeGuest = (Button) event.getSource();
        Stage stage = (Stage) backChangeGuest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestcenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestCenter backChangeGu = loader.getController();
        backChangeGu.setUser(user);
        stage.show();
    }
    @FXML
    public void confirmBtnChangeGuestOnAction(ActionEvent event) throws IOException {
        if (oldPasswordGuest.getText().equals("") || passwordChangeGuest.getText().equals("") || passwordAgainChangeGuest.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Username or Password");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        } else {
            if (user.getCurrentAccount().getPassword().equals(oldPasswordGuest.getText())) {
                if (passwordChangeGuest.getText().equals(passwordAgainChangeGuest.getText())) {
                    user.getCurrentAccount().setPassword(passwordChangeGuest.getText());
                    UserFileDataSource userData = new UserFileDataSource("data", "loginguest.csv");
                    userData.setUserList(user);
                    Alert alert = new Alert(Alert.AlertType.NONE,"Change Guest Password Done.", ButtonType.OK);
                    alert.showAndWait();
                    Button confirmChangePassGuest = (Button) event.getSource();
                    Stage stage = (Stage) confirmChangePassGuest.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestlogin.fxml"));

                    stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                    GuestLogin confirmChangeGu = loader.getController();
                    stage.show();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong Password.");
                    alert.setHeaderText(null);
                    alert.setContentText("Please Enter the same password.");
                    alert.showAndWait();
                    passwordChangeGuest.clear();
                    passwordAgainChangeGuest.clear();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Wrong Password.");
                alert.setHeaderText(null);
                alert.setContentText("Please Enter the correct old Password.");
                alert.showAndWait();
                oldPasswordGuest.clear();
            }
        }
    }
}
