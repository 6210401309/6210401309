package controller;

import javafx.scene.control.*;
import model.User;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class AdminLogin {

    private UserFileDataSource userData;
    private User userList;
    private User user;
    @FXML Button homeBtnAdmin,enterBtn;
    @FXML PasswordField passwordAdminField;
    @FXML TextField usernameAdminField;

    @FXML public void initialize(){
        userData = new UserFileDataSource("data","loginadmin.csv");
        userList= userData.getUserData();
    }
    @FXML
    public void homeBtnAdminOnAction(ActionEvent event) throws IOException {
        Button backHomeAdmin = (Button) event.getSource();
        Stage stage = (Stage) backHomeAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome homeAd = loader.getController();
        stage.show();
    }
    @FXML
    public void enterBtnOnAction(ActionEvent event) throws IOException {
        if(usernameAdminField.getText().equals("")||passwordAdminField.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Wrong Username or Password");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        }
        else {
            if (userList.login(usernameAdminField.getText(), passwordAdminField.getText())) {
                Button enterAdmin = (Button) event.getSource();
                Stage stage = (Stage) enterAdmin.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincenter.fxml"));
                stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                AdminCenter enterAd = loader.getController();
                enterAd.setUser(userList);
                stage.show();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong Username or Password");
                alert.setHeaderText(null);
                alert.setContentText("Please enter correct information.");
                alert.showAndWait();
            }
            usernameAdminField.clear();
            passwordAdminField.clear();
        }
    }
    public void setUser(User user) {
        this.user = user;
    }
}
