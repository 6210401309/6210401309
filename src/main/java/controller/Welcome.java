package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Welcome {
    @FXML Button producerFirstBtn,adminFirstBtn,staffFirstBtn,guestFirstBtn,AdviceFirstBtn;
    @FXML
    public void producerFirstBtnOnAction(ActionEvent event) throws IOException {
        Button producer = (Button) event.getSource();
        Stage stage = (Stage) producer.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/producerpage.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        ProducerPage pro = loader.getController();
        stage.show();
    }

    @FXML
    public void adminFirstBtnOnAction(ActionEvent event) throws IOException {
        Button admin = (Button) event.getSource();
        Stage stage = (Stage) admin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminlogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminLogin ad = loader.getController();
        stage.show();
    }

    @FXML
    public void staffFirstBtnOnAction(ActionEvent event) throws IOException {
        Button staff = (Button) event.getSource();
        Stage stage = (Stage) staff.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/stafflogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        StaffLogin staf = loader.getController();
        stage.show();
    }
    @FXML
    public void guestFirstBtnOnAction(ActionEvent event) throws IOException {
        Button guest = (Button) event.getSource();
        Stage stage = (Stage) guest.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/guestlogin.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        GuestLogin gue = loader.getController();
        stage.show();
    }
    @FXML
    public void AdviceFirstBtnOnAction(ActionEvent event) throws IOException {
        Button advice = (Button) event.getSource();
        Stage stage = (Stage) advice.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/advice.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Advice adv = loader.getController();
        stage.show();
    }
}
