package controller;

import javafx.scene.control.*;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;

public class AdminCreateStaff {
    private User user;
    private UserFileDataSource userData;
    private User userList;
    @FXML
    Button backBtnAdminCreateStaff, logoutBtnAdminCreateStaff, addCreateStaff;
    @FXML
    TextField nameNewStaff, usernameNewStaff;
    @FXML
    PasswordField passwordNewStaff, passwordAgainNewStaff;
    @FXML
    Label nameLabelAdmin;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelAdmin.setText(user.getCurrentAccount().getName());
            }
        });
    }

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    public void logoutBtnAdminCreateStaffOnAction(ActionEvent event) throws IOException {
        Button logoutCreateStaffAdmin = (Button) event.getSource();
        Stage stage = (Stage) logoutCreateStaffAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutCreateAd = loader.getController();
        stage.show();
    }

    public void backBtnAdminCreateStaffOnAction(ActionEvent event) throws IOException {
        Button backCreateStaffAdmin = (Button) event.getSource();
        Stage stage = (Stage) backCreateStaffAdmin.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminCenter backCreateStaffAd = loader.getController();
        backCreateStaffAd.setUser(user);
        stage.show();
    }

    @FXML
    public void addCreateStaffOnAction(ActionEvent event) {
        userData = new UserFileDataSource("data", "loginstaff.csv");
        userList = userData.getUserData();
        if (nameNewStaff.getText().equals("") || usernameNewStaff.getText().equals("") || passwordNewStaff.getText().equals("") || passwordAgainNewStaff.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Missing Information");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        }
        else {
            if (user.getCurrentAccount().getUsername().equals(usernameNewStaff.getText()) && user.getCurrentAccount().getPassword().equals(passwordNewStaff.getText())) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Use Another Password");
                alert.setHeaderText(null);
                alert.setContentText("Please use another Username or Password.");
                alert.showAndWait();
            }
            else {
                if (userList.login(usernameNewStaff.getText(), passwordNewStaff.getText())) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Use Another Username or Password");
                    alert.setHeaderText(null);
                    alert.setContentText("Please use another Username or Password.");
                    alert.showAndWait();
                }
                else {
                    if (passwordNewStaff.getText().equals(passwordAgainNewStaff.getText())) {
                        userList.addUser(nameNewStaff.getText(), usernameNewStaff.getText(), passwordNewStaff.getText());
                        Alert alert = new Alert(Alert.AlertType.NONE,"Create Staff Done.",ButtonType.OK);
                        alert.showAndWait();
                        nameNewStaff.clear();
                        usernameNewStaff.clear();
                        passwordNewStaff.clear();
                        passwordAgainNewStaff.clear();
                    } else {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Wrong Username or Password");
                        alert.setHeaderText(null);
                        alert.setContentText("Please enter the same Password!!.");
                        alert.showAndWait();
                    }
                    userData.setUserList(userList);
                }
            }
        }
    }
}
