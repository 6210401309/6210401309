package controller;

import javafx.scene.control.*;
import service.UserFileDataSource;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.User;

import java.io.IOException;

public class AdminChangePassword {
    private User user;
    private UserFileDataSource userData;
    @FXML
    Button backBtnAdminChangePassword,logoutBtnAdminChangePassword,confirmBtnChangeAdmin;
    @FXML
    Label nameLabelAdmin;
    @FXML
    PasswordField oldPasswordAdmin,passwordChangeAdmin,passwordAgainChangeAdmin;
    @FXML public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                nameLabelAdmin.setText(user.getCurrentAccount().getName());
            }
        });
    }

    @FXML
    public void logoutBtnAdminChangePasswordOnAction(ActionEvent event) throws IOException {
        Button logoutAdminThird = (Button) event.getSource();
        Stage stage = (Stage) logoutAdminThird.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/welcome.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        Welcome logoutAdThird = loader.getController();
        stage.show();
    }
    @FXML
    public void backBtnAdminChangePasswordOnAction(ActionEvent event) throws IOException {
        Button backAdminThird = (Button) event.getSource();
        Stage stage = (Stage) backAdminThird.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/admincenter.fxml"));

        stage.setScene(new Scene((Parent) loader.load(), 800, 600));
        AdminCenter backThirdAd = loader.getController();
        backThirdAd.setUser(user);
        stage.show();
    }
    @FXML
    public void confirmBtnChangeAdminOnAction(ActionEvent event) throws IOException {
        if (oldPasswordAdmin.getText().equals("") || passwordChangeAdmin.getText().equals("") || passwordAgainChangeAdmin.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Missing Information");
            alert.setHeaderText(null);
            alert.setContentText("Please complete all information!!");
            alert.showAndWait();
        } else {
            if (user.getCurrentAccount().getPassword().equals(oldPasswordAdmin.getText())) {
                if (passwordChangeAdmin.getText().equals(passwordAgainChangeAdmin.getText())) {
                    user.getCurrentAccount().setPassword(passwordChangeAdmin.getText());
                    UserFileDataSource userData = new UserFileDataSource("data", "loginadmin.csv");
                    userData.setUserList(user);
                    Button confirmChangePassAdmin = (Button) event.getSource();
                    Stage stage = (Stage) confirmChangePassAdmin.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminlogin.fxml"));

                    stage.setScene(new Scene((Parent) loader.load(), 800, 600));
                    AdminLogin confirmChangeAdmin = loader.getController();
                    stage.show();
                    Alert alert = new Alert(Alert.AlertType.NONE,"Change Password Done.", ButtonType.OK);
                    alert.showAndWait();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Wrong Password.");
                    alert.setHeaderText(null);
                    alert.setContentText("Please Enter the same password.");
                    alert.showAndWait();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Wrong Password.");
                alert.setHeaderText(null);
                alert.setContentText("Please Enter the correct old Password.");
                alert.showAndWait();
            }
        }
    }
        public void setUser (User user){
            this.user = user;
        }
}
