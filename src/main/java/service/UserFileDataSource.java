package service;

import model.User;

import java.io.*;

public class UserFileDataSource {
    private String fileDirectoryUser;
    private String fileUser;
    private User userList;

    public UserFileDataSource(String fileDirectoryUser, String fileUser) {
        this.fileDirectoryUser = fileDirectoryUser;
        this.fileUser = fileUser;
        checkFileUserIsExisted();
    }
    private void checkFileUserIsExisted() {
        File fileuser = new File(fileDirectoryUser);
        if (!fileuser.exists()) {
            fileuser.mkdirs();
        }
        String filePath = fileDirectoryUser + File.separator + fileUser;
        fileuser = new File(filePath);
        if (!fileuser.exists()) {
            try {
                fileuser.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryUser + File.separator + fileUser;
        File fileuser = new File(filePath);
        FileReader fileReader = new FileReader(fileuser);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            User user = new User(data[0], data[1], data[2]);
            if(data.length==4){
                user.setTime(data[3]);
            }
            userList.add(user);
        }
        reader.close();
    }



    public User getUserData() {
        try {
            userList = new User();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileUser + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileUser);
        }
        return userList;
    }

    public void setUserList(User user) {
        String filePath = fileDirectoryUser + File.separator + fileUser;
        File fileuser = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileuser);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (User newUser: user.getUserBox()) {
                String line ="";
                line =newUser.getName()+","+newUser.getUsername()+","+newUser.getPassword();
                if(newUser.getTime()!=null){
                    line+=","+newUser.getTime();
                }
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

}
