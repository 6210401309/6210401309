package service;

import model.Document;
import model.Letter;
import model.Mailbox;
import model.Parcel;

import java.io.*;

public class MailFileDataSource {
    private String fileDirectoryName;
    private String fileName;
    private Mailbox mailList;

    public MailFileDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Mailbox mail = null;
            if(data[0].equals("Letter")){
                mail = new Letter(data[0],data[1], Integer.parseInt(data[2]),data[3],data[4],data[5]);
                if(data.length==7){
                    mail.setTime(data[6]);
                }
            }else if(data[0].equals("Document")){
                mail = new Document(data[0],data[1],Integer.parseInt(data[2]),data[3],data[4],data[5],data[6]);
                if(data.length==8){
                    mail.setTime(data[7]);
                }
            }
            else if(data[0].equals("Parcel")){
                mail = new Parcel(data[0],data[1],Integer.parseInt(data[2]),data[3],data[4],data[5],data[6],data[7]);
                if(data.length==9){
                    mail.setTime(data[8]);
                }
            }
            mailList.add(mail);
        }
        reader.close();
    }

    public Mailbox getMailData() {
        try {
            mailList = new Mailbox();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileName + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileName);
        }
        return mailList;
    }
    public void setMailList(Mailbox mail) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Mailbox newMail: mail.getBox()) {
                String line ="";
                if(newMail.getType().equals("Letter")){
                    Letter letter = (Letter) newMail;
                    line="Letter,"+letter.getRecipient()+","+letter.getNumRoom()+","+letter.getDeliverer()+","
                            +letter.getSize()+","+letter.getNameStaffAddMail();
                    if(newMail.getTime()!=null){
                        line+=","+newMail.getTime();
                    }
                }else if(newMail.getType().equals("Document")){
                    Document document = (Document) newMail;
                    line = "Document,"+document.getRecipient()+","+document.getNumRoom()+","+document.getDeliverer()+","
                            +document.getSize()+","+document.getNameStaffAddMail()+","+document.getImportant();
                    if(newMail.getTime()!=null){
                        line+=","+newMail.getTime();
                    }
                }else if(newMail.getType().equals("Parcel")){
                    Parcel parcel = (Parcel) newMail;
                    line = "Parcel,"+parcel.getRecipient()+","+parcel.getNumRoom()+","+parcel.getDeliverer()+","
                            +parcel.getSize()+","+parcel.getNameStaffAddMail()+","+parcel.getService()+","+parcel.getTrackingNumber();
                    if(newMail.getTime()!=null){
                        line+=","+newMail.getTime();
                    }
                }
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
