package service;

import model.Extraroom;
import model.Room;

import java.io.*;

public class RoomFileDataSource {
    private String fileDirectoryRoom;
    private String fileRoom;
    private Room roomList;

    public RoomFileDataSource(String fileDirectoryRoom,String fileRoom){
        this.fileDirectoryRoom = fileDirectoryRoom;
        this.fileRoom = fileRoom;
        checkFileRoomIsExisted();
    }

    private void checkFileRoomIsExisted() {
        File filerooms = new File(fileDirectoryRoom);
        if (!filerooms.exists()) {
            filerooms.mkdirs();
        }
        String filePath = fileDirectoryRoom + File.separator + fileRoom;
        filerooms = new File(filePath);
        if (!filerooms.exists()) {
            try {
                filerooms.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }
    private void readData() throws IOException {
        String filePath = fileDirectoryRoom + File.separator + fileRoom;
        File filerooms = new File(filePath);
        FileReader fileReader = new FileReader(filerooms);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            if(data[0].equals("Normal")) {
                Room room = new Room(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[4], data[5], data[6], data[7]);
                roomList.add(room);
            }
            else if (data[0].equals("Extra")){
                Room room = new Extraroom(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), Integer.parseInt(data[3]), data[4], data[5], data[6], data[7],data[8],data[9]);
                roomList.add(room);
            }
        }
        reader.close();
    }

    public Room getRoomData() {
        try {
            roomList = new Room();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileRoom + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileRoom);
        }
        return roomList;
    }

    public void setRoomList(Room room) {
        String filePath = fileDirectoryRoom + File.separator + fileRoom;
        File fileroom = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileroom);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Room newRoom: room.getRooms()) {
                String line = "";
                if (newRoom.getTypeRoom().equals("Normal")) {
                    Room rooms = (Room) newRoom;
                    line = "Normal" + "," + rooms.getBuilding() + "," + rooms.getLevel() + "," + rooms.getNumRoom() + "," + rooms.getName() + "," + rooms.getUsername() + ","
                            + rooms.getPassword() + "," + rooms.CheckPassword();
                }
                if (newRoom.getTypeRoom().equals("Extra")) {
                    Extraroom extraroom = (Extraroom) newRoom;
                    line = "Extra" + "," + extraroom.getBuilding() + "," + extraroom.getLevel() + "," + extraroom.getNumRoom() + "," + extraroom.getName() + "," + extraroom.getUsername() + ","
                            + extraroom.getPassword() + "," + extraroom.CheckPassword() + "," + extraroom.getSecondName() + "," + extraroom.getThirdName();
                }
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }

}
