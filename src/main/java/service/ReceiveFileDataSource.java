package service;

import model.Mailbox;
import model.ReceiveDocument;
import model.ReceiveLetter;
import model.ReceiveParcel;

import java.io.*;

public class ReceiveFileDataSource {
    private String fileDirectoryReceive;
    private String fileReceive;
    private Mailbox receiveList;

    public ReceiveFileDataSource(String fileDirectoryReceive, String fileReceive) {
        this.fileDirectoryReceive = fileDirectoryReceive;
        this.fileReceive = fileReceive;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryReceive);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryReceive + File.separator + fileReceive;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create " + filePath);
            }
        }
    }

    private void readData() throws IOException {
        String filePath = fileDirectoryReceive + File.separator + fileReceive;
        File file = new File(filePath);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line = "";
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            Mailbox receives = null;
            if(data[0].equals("Letter")){
                receives = new ReceiveLetter(data[0],data[1], Integer.parseInt(data[2]),data[3],data[4],data[5],data[6],data[7],data[8],data[9]);
            }else if(data[0].equals("Document")){
                receives = new ReceiveDocument(data[0],data[1],Integer.parseInt(data[2]),data[3],data[4],data[5],data[6],data[7],data[8],data[9],data[10]);
            }
            else if(data[0].equals("Parcel")){
                receives = new ReceiveParcel(data[0],data[1],Integer.parseInt(data[2]),data[3],data[4],data[5],data[6],data[7],data[8],data[9],data[10],data[11]);
            }
            receiveList.add(receives);
        }
        reader.close();
    }

    public Mailbox getReceiveData() {
        try {
            receiveList = new Mailbox();
            readData();
        } catch (FileNotFoundException e) {
            System.err.println(this.fileReceive + " not found");
        } catch (IOException e) {
            System.err.println("IOException from reading " + this.fileReceive);
        }
        return receiveList;
    }
    public void setReceiveList(Mailbox receives) {
        String filePath = fileDirectoryReceive + File.separator + fileReceive;
        File file = new File(filePath);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (Mailbox newReceive: receives.getBox()) {
                String line ="";
                if(newReceive.getType().equals("Mail")){
                    ReceiveLetter letter = (ReceiveLetter) newReceive;
                    line="Letter,"+letter.getRecipient()+","+letter.getNumRoom()+","+letter.getDeliverer()+","
                            +letter.getSize()+","+letter.getNameStaffAddMail()+","+letter.getNameGuest()+","+letter.getNameStaffReceiveMail()+","+letter.getReceiveTime()+","+letter.getTime();
                }else if(newReceive.getType().equals("Document")){
                    ReceiveDocument document = (ReceiveDocument) newReceive;
                    line = "Document,"+document.getRecipient()+","+document.getNumRoom()+","+document.getDeliverer()+","
                            +document.getSize()+","+document.getNameStaffAddMail()+","+document.getNameGuest()+","+document.getNameStaffReceiveMail()+","+document.getImportant()+","+document.getReceiveTime()+","+document.getTime();

                }else if(newReceive.getType().equals("Parcel")){
                    ReceiveParcel parcel = (ReceiveParcel) newReceive;
                    line = "Parcel,"+parcel.getRecipient()+","+parcel.getNumRoom()+","+parcel.getDeliverer()+","
                            +parcel.getSize()+","+parcel.getNameStaffAddMail()+","+parcel.getNameGuest()+","+parcel.getNameStaffReceiveMail()+","+parcel.getService()+","+parcel.getTrackingNumber()+","+parcel.getReceiveTime()+","+parcel.getTime();
                }
                writer.append(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Cannot write " + filePath);
        }
    }
}
